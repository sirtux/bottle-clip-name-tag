/*
 * Examples for bottle clip name tags.
 * Copyright (C) 2013 Roland Hieber <rohieb+bottleclip@rohieb.name>
 *
 * You can simply comment out one of the examples, adapt it to your needs and
 * render it, or use it to build your own variant.
 *
 * The contents of this file are licenced under CC-BY-SA 3.0 Unported.
 * See https://creativecommons.org/licenses/by-sa/3.0/deed for the
 * licensing terms.
 */

$fn=500;

use <tux-ring-hohl.scad>

difference() {
    
empty_bottle_clip();
//bottle_clip(name="SirTux", font="write/orbitron.dxf", logo="thing-logos/pesthoernchen.dxf");


}
// vim: set noet ts=2 sw=2 tw=80:
